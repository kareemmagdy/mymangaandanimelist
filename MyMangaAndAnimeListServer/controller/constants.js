module.exports= {
    ITEM_DELETED: "item has benn deleted",
    ITEMS_RETREIVED:"items have been retrieved",
    ITEM_RETREIVED:"item has been retrieved",
    ITEMS_UPDATED: "Follow list updated",
    ITEM_ADDED:"item has been added to your following list",
    NEW_ITEMS_ADDED: "new items have been added to the database",
    SUCCESSFULLY_REQUEST_CODE: 200, 
    SERVER_ERROR_CODE: 500,
    BAD_REQUEST_CODE: 400,
}