const objService = require('../service/show');
const constants = require('./constants');

module.exports.GetNewShows = (req, res) => {
    var { error, result } = objService.GetNewShowsService();
    if (error) {
        err = error;
        return res.status(constants.SERVER_ERROR_CODE).json(new Responses(error, []));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.NEW_ITEMS_ADDED, result));
}

module.exports.AddShow = (req, res) => {
    var { show_id } = req.body;
    var { error, result } = objService.AddShowService(show_id);

    if (error) {
        err = error;
        return res.status(constants.SERVER_ERROR_CODE).json(new Responses(error, []));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.NEW_ITEMS_ADDED, result));
}