const FollowListModel = requier('../model/follow_list.js');
const Responses = require('../responses/response');
const constants = require('./constants');
const objService = require('../service/follow_list');

module.exports.GetFollowList = (req, res) => {
    let followList = {};
    var { error, result } = FollowListModel.GetFollowList();

    if (error) {
        return res.status(constants.SERVER_ERROR_CODE).json(new Responses(err));
    }
    followList = result;
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEMS_RETREIVED, followList));
}

module.exports.addComic = (req, res) => {
    let comic = {};

    var { comic_id } = req.body;
    var { error, result } = objService.AddComicService(comic_id);
    if (error) {
        return res.status(constants.BAD_REQUEST_CODE).json(new Responses(error, comic));
    }
    comic = result;
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEM_ADDED, comic));
}

module.exports.addShow = (req, res) => {
    let show = {};

    var { show_id } = req.body;
    var { error, result } = objService.AddShowService(show_id);
    if (error) {
        return res.status(constants.BAD_REQUEST_CODE).json(new Responses(error, show));
    }
    show = result;
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEM_ADDED, show));
}

module.exports.RemoveComic = (req, res) => {
    let comic = {};
    var { comic_id } = req.body;
    var { error, result } = objService.RemoveComicService(comic_id);
    if (error) {
        return res.status(constants.BAD_REQUEST_CODE).json(new Responses(error, comic));
    }
    comic = result;
    res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEM_ADDED, comic));
}

module.exports.RemoveShow = (req, res) => {
    var { show_id } = req.body;
    var { error, result } = objService.RemoveShowService(show_id);
    if (error) {
        return res.status(constants.BAD_REQUEST_CODE).json(new Responses(error));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEM_DELETED, result));
}

module.exports.UpdateFollowList = (req, res) => {
    var error = objService.UpdateFollowListService();
    if (error) {
        return res.status(constants.BAD_REQUEST_CODE).json(new Responses(err));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.ITEMS_UPDATED));
}