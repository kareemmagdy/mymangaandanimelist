const objService = require('../service/comic');
const constants = require('./constants');
const Responses = require('../responses/response')
module.exports.GetNewComics = (req, res) => {
    var { error, result } = objService.GetNewComicsService();
    if (error) {
        err = error;
        return res.status(constants.SERVER_ERROR_CODE).json(new Responses(error, []));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.NEW_ITEMS_ADDED, result));
}

module.exports.AddComic = async (req, res) => {
    var { comic_name } = req.body;
    var { error, result } = await objService.AddComicService(comic_name);

    if (error) {
        err = error;
        return res.status(constants.SERVER_ERROR_CODE).json(new Responses(err, []));
    }
    return res.status(constants.SUCCESSFULLY_REQUEST_CODE).json(new Responses(constants.NEW_ITEMS_ADDED, result));
}