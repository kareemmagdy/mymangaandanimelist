const express = require('express');
const router = express.Router();

const objController = require( '../controller/show');

router.post('/refetch', objController.GetNewShows);
router.post('/show',objController.AddShow);