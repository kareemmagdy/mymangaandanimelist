const express = require('express');
const router = express.Router();

const objController = require('../controller/comic');

router.post('/refetch', objController.GetNewComics);
router.post('/comic', objController.AddComic);

module.exports = router;