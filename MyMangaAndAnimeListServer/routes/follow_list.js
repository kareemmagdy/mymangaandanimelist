const express = require('express');
const router = express.Router();
const objController = require( '../controller/follow_list');

router.post('/comic',objController.addComic);
router.post('/show'.objController.addShow);
router.patch('/comic',objController.RemoveComic);
router.patch('/show',objController.RemoveShow);
router.post('/update',objController.UpdateFollowList)
router.get('/', objController.GetFollowList)