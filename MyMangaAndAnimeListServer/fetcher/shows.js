const axios = require('axios')

module.exports.Fetch = async (show_name) => {
    let err = null;
    let show = {};
    try {
        axios
            .post('http://localhost:9090/scrapper/show/', {
                show_name: show_name
            })
            .then((res) => {
                if (res.status == 200) {
                    show = res.result;
                } else {
                    err = res.err;
                }
            })
            .catch(error => {
                err = error;
            })
    } catch (error) {
        err = error;
    }
    return { err, show };
}

module.exports.FetchNewShows = async () => {
    let err = null;
    let shows = []
    try {
        axios.post('http://localhost:9090/scrapper/shows/', {})
            .then(res => {
                if (res.status == 200) {
                    shows = res.shows;
                } else {
                    err = res.err;
                }
            })
            .catch(error => {
                err = error;
            })
    } catch (error) {
        err = error;
    }
    return {err , shows};
}