const axios = require('axios')

module.exports.Fetch = async (comic_name) => {
    let err = null;
    let comic = {};
    try {
        let res = await axios.post('http://localhost:3000/scrapper/comic/', { comic_name: comic_name });
        if (res.status == 200) {
            comic = res.data.comic;
        } else {
            err = res.err;
        }
        return { err, comic };
    } catch (error) {
        err = error;
        console.log(err);
        return { err, comic };
    }
}