const mongoose = require('mongoose');
const helper = require('../helper/helper.js');

const ShowSchema = new mongoose.Schema({
    names: [
        {
            type: String,
            unique: true,
        }
    ],
    sources: [
        {
            type: String,
            required: true,
        }
    ],
    latest: {
        type: String,
        required: true,
    },
    profilePic: {
        type: String,
    },
    expectedLength: {
        type: String,
        required: false,
    },
    studio: [
        {
            type: String,
            required: true,
        }
    ]
});


ShowSchema.methods.AddShows = function (arrOfShows) {
    let shows = [];
    let err = null;
    //assumes that arrOfShows is literally an array of showObjects
    for (let index = 0; index < arrOfShows.length; index++) {
        this.save(function (err, show) {
            if (err) {
                console.log(err);
                return { err, shows }
            }
            shows.push(show);
        })
    }
    return { err, comics: shows };
}

ShowSchema.methods.GetShow = function (show_id) {
    let err = null;
    let show = {};
    this.findById(show_id, function (err, retrievedShow) {
        if (err) {
            console.log(err);
            return { err, show };
        }
        show = retrievedShow;
        return { err, show };
    })
}
// TvSeriesSchema.methods.GetLatest = function () {
//     return this.password == password;
// }

const Show = mongoose.model("Show", ShowSchema);

module.exports = Show;