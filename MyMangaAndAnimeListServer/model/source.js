const mongoose = require('mongoose');
const helper = require('../helper/helper.js');

const SourceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    link: {
        type: String,
        required: true,
        unique: true
    },
    status: {
        type: ["online", "down"],
    }
});

SourceSchema.methods.CheckStatus = function () {
    this.status = helper.CheckWebsiteStatus(this.link);
}

const Source = mongoose.model("Source", SourceSchema);

module.exports = Source;