const mongoose = require('mongoose');
const helper = require('../helper/helper.js');

const ComicsSchema = new mongoose.Schema({
    names: [
        {
            type: String,
            unique: true,
        }
    ],
    sources: [
        {
            type: String,
            required: true,
        }
    ],
    latest: {
        type: String,
        required: true,
    },
    picture: {
        type: String,
    },
    author: {
        type: String,
        required: true
    },
});

// ComicsSchema.methods.AddComics = function (arrOfComics) {
//     let comics = [];
//     let err = null;
//     for (let index = 0; index < arrOfComics.length; index++) {
//         if (arrOfComics[index].length > 0) {
//             await this.save(function (err, comic) {
//                 if (err) {
//                     console.log(err);
//                     return { err, comics }
//                 }
//                 comics.push(comic);
//             })
//         }
//     }
//     return { err, comics };
// }

// ComicsSchema.methods.GetComic = function (comic_id) {
//     let err = null;
//     let comic = {};
//     this.findById(comic_id, function (err, retrievedComic) {
//         if (err) {
//             console.log(err);
//             return { err, comic };
//         }
//         comic = retrievedComic;
//         return { err, comic };
//     })
// }


// TvSeriesSchema.methods.GetLatest = function () {
//     return this.password == password;
// }

const Comics = mongoose.model("Comic", ComicsSchema);

module.exports = Comics;