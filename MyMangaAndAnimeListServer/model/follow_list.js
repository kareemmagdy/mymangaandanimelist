const mongoose = require('mongoose');
const FollowListSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
        unique: true
    },
    TvSeriesList:[ {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Show",
        unique: true,
    }],
    ComicsList: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comic",
        unique: true,
    }],
});

// UserSchema.methods.validPassword = function (password) {

//     return this.password == password;
// }

const FollowList = mongoose.model("FollowList", FollowListSchema);

module.exports = FollowList;