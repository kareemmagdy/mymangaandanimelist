
module.exports = class Response {
    constructor(message, result) {
        this.message = message;
        if (result) {
            this.result = result;
        }
    }
}