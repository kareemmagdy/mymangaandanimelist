const objFollowListModel = require('../model/follow_list');
const objComicsModel = require('../model/comic');
const objShowsModel = require('../model/show');
const objComicsFetcher = require('../fetcher/comics');
const objShowsFetcher = require('../fetcher/shows')

module.exports.GetFollowListService = () => {
    let result = {};
    let error = null;
    var { err, followList } = objFollowListModel.GetFollowListItems();
    if (err) {
        error = err
        return { error, result };
    }
    result = followList;
    return { error, result };
}

module.exports.AddComicService = (comic_id) => {
    let error = null;
    let result = {};

    var { err, comic } = objComicsModel.GetComic(comic_id)
    if (err) {
        error = err;
        return { error, result };
    }

    var { err, comic } = objFollowListModel.AddComic(comic);
    if (err) {
        error = err;
        return { error, result };
    }
    result = comic;
    return { error, result };
}

module.exports.AddShowService = (show_id) => {
    let error = null;
    let result = {};

    var { err, show } = objShowsModel.GetShow(show_id);
    if (err) {
        error = err;
        return { error, result };
    }

    var { err, show } = objFollowListModel.AddShow(result);
    if (err) {
        error = err;
        return { error, result };
    }
    result = show;
    return { error, result };
}

module.exports.RemoveComicService = (comic_id) => {
    let error = null;
    let result = {};
    var { err, comic } = objFollowListModel.RemoveComic(comic_id);
    if (err) {
        error = err;
        return { error, result };
    }
    result = comic;
    return { error, result };
}

module.exports.RemoveShowService = (show_id) => {
    let error = null;
    let result = {};
    var { err, show } = objFollowListModel.RemoveShow(show_id);
    if (err) {
        error = err;
        return { error, result };
    }
    result = show;
    return { error, result };
}

module.exports.UpdateFollowListService = () => {
    let error = null;
    var { err, comics } = objFollowListModel.GetComics()
    if (err) {
        return err;
    }
    var { err, comics } = objComicsFetcher.UpdateComics(comics);
    if (err) {
        return err;
    }

    var { err, shows } = objFollowListModel.GetShows();
    if (err) {
        return err;
    }
    var { err, shows } = objShowsFetcher.UpdateShows(shows);
    if (err) {
        return err;
    }
    return error;
}



