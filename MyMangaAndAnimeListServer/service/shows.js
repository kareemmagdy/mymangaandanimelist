const objShowsModel = require('../model/show');
const objShowsFetcher = require('../fetcher/shows')

module.exports.GetNewShowsService = () => {
    let error = null;
    let result = [];

    var { err, shows } = objShowsFetcher.FetchNewShows();
    if (err) {
        error = err;
        return { error, result }
    }

    var { err, shows } = objShowsModel.AddShows(result);
    if (err) {
        error = err;
        return { error, result };
    }
    result = shows;
    return { error, result };
}

module.exports.AddShowService = (show_id) => {
    let error = null;
    let result = {};

    var { err, show } = objShowsFetcher.Fetch(show_id);
    if (err) {
        error = err;
        return { error, result };
    }

    var { err, show } = objFollowListModel.AddComic(result);
    if (err) {
        error = err;
        return { error, result };
    }
    result = show;
    return { error, result };
}
