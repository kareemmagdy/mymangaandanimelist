const objComicsModel = require('../model/comic');
const objComicsFetcher = require('../fetcher/comics')

module.exports.GetNewComicsService = () => {
    let error = null;
    let result = [];

    var { err, comics } = objComicsFetcher.FetchNewComics();
    if (err) {
        error = err;
        return { error, result }
    }

    var { err, comics } = objComicsModel.AddComics(result);
    if (err) {
        error = err;
        return { error, result };
    }
    result = comics;
    return { error, result };
}


module.exports.AddComicService = async (comic_name) => {
    let error = null;
    let result = {};

    var { err, comic } = await objComicsFetcher.Fetch(comic_name);
    if (err) {
        error = err;
        return { error, result };
    }
    
    try {
        let objComic = new objComicsModel(comic);
        let savedUser = await objComic.save()
        result = savedUser;
    } catch (err) {
        error = "this validates constraints";
        console.log(err.message);
    }
    return { error, result };
}