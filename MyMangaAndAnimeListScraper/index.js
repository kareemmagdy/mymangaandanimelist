const express = require("express")
const app = express()
require("dotenv").config();

const comicRoutes=require("./app/admin/comics/route");

app.use(express.json());

app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader("Access-Control-Allow-Methods","GET , POST , DELETE");
    res.setHeader("Access-Control-Allow-Headers","Content-Type, Authorization");
    next();
})

app.use("/scrapper/",comicRoutes);

app.listen(3000);