const axios = require("axios");
const cheerio = require("cheerio");

exports.LoadHtml = async (url) => {
    try {
        const {data} = await axios.get(url);
        const $ = cheerio.load(data);
        return $;

    } catch (err) {
        console.error(err.message);
    }
};


async function getElements(url, selector) {
    try {
        // Load HTML we fetched in the previous line
        const $ = await this.LoadHtml(url);
        if (!$) {
            return [];
        }
        // Select all the list items  based on selector
        const listItems = await $(selector);
        // Logs countries array to the console
        return {listItems, $};

    } catch (err) {
        console.error(err.message);
        return [];
    }
}

exports.GetFromHtmlBySelector = async ($, selector, text_type) => {
    switch (text_type) {
        case "text":
            return $(selector).text();
        case "href":
            return $(selector).attr('href');
        case "src":
            return $(selector).attr('src');
        case "srcset":
            return $(selector).attr('srcset');
        default:
            return $(selector).text();
    }
};