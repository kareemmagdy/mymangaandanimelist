const objService = require('./service');

exports.GetNewComics = async (req,res) => {
    
}

exports.GetComics = async (req,res) => {

}

exports.GetComic = async (req,res) => {
    var {comic_name} = req.body;
    var {err , comic}=await objService.GetComicService(comic_name);
    if(err){
        return res.status(400).json({'Message':err});
    }
    return res.status(200).json({"comic": comic});
}