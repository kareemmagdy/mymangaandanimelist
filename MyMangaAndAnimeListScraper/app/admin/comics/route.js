

const express = require('express');
const router = express.Router();

const objController = require( './controller');


router.post('/comic/', objController.GetComic);
router.post('/comis/', objController.GetComics);
router.post('/refetch/comics/', objController.GetNewComics);

module.exports= router;