

const express = require('express');
const router = express.Router();

const objController = require('./controller');


router.post('/show/', objController.GetShow);
router.post('/shows/', objController.GetShows);
router.post('/refetch/shows/', objController.GetNewShows);

module.exports = router;
