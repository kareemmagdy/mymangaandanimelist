
const objScrapper = require('../../helper/scrapper');
const constants = require('./constant');

exports.Fetch = async (comic_name) => {
    let comic = {};
    let err = null;
    try {
        let url = constants.WEBSITE_URL + comic_name;
        let page = await objScrapper.LoadHtml(url);
        let title = await objScrapper.GetFromHtmlBySelector(page, "._3xnDj");
        
        let picture = await objScrapper.GetFromHtmlBySelector(page, ".img-responsive", 'src');
        let latestChapter = await objScrapper.GetFromHtmlBySelector(page, "._2wcqV > div:nth-child(2) > div:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1) > span:nth-child(1)");
        let author = await objScrapper.GetFromHtmlBySelector(page, "._3QCtP > div:nth-child(2) > div:nth-child(1) > span:nth-child(2)");
        comic['names'] = title.split(';');
        comic['sources'] = [url];
        comic['latest'] = latestChapter;
        comic['picture'] = picture;
        comic['author'] = author;
    } catch (error) {
        err = error;
        console.log(error);
        return { err, comic };
    }

    return { err, comic };
}